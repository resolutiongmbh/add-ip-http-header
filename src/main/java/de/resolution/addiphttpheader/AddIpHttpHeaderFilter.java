package de.resolution.addiphttpheader;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class AddIpHttpHeaderFilter implements javax.servlet.Filter {

    static private final String ALREADY_FILTERED = AddIpHttpHeaderFilter.class.getName() + "_already_filtered";

    /**
     * Called by the web container to indicate to a filter that it is being placed into service.
     *
     * <p>The servlet container calls the init
     * method exactly once after instantiating the filter. The init method must complete successfully before the filter
     * is asked to do any filtering work.
     *
     * <p>The web container cannot place the filter into service if the init
     * method either
     * <ol>
     * <li>Throws a ServletException
     * <li>Does not return within a time period defined by the web container
     * </ol>
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // nop
    }

    /**
     * The <code>doFilter</code> method of the Filter is called by the container each time a request/response pair is
     * passed through the chain due to a client request for a resource at the end of the chain. The FilterChain passed
     * in to this method allows the Filter to pass on the request and response to the next entity in the chain.
     *
     * <p>A typical implementation of this method would follow the following
     * pattern:
     * <ol>
     * <li>Examine the request
     * <li>Optionally wrap the request object with a custom implementation to
     * filter content or headers for input filtering
     * <li>Optionally wrap the response object with a custom implementation to
     * filter content or headers for output filtering
     * <li>
     * <ul>
     * <li><strong>Either</strong> invoke the next entity in the chain
     * using the FilterChain object (<code>chain.doFilter()</code>),
     * <li><strong>or</strong> not pass on the request/response pair to
     * the next entity in the filter chain to block the request processing
     * </ul>
     * <li>Directly set headers on the response after invocation of the
     * next entity in the filter chain.
     * </ol>
     *
     * @param request
     * @param response
     * @param chain
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (request.getAttribute(ALREADY_FILTERED) != null) {
            chain.doFilter(request, response);
            return;
        }
        request.setAttribute(ALREADY_FILTERED, "yes");

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletRequestWrapper wrappedRequest = new OverrideHeaderWrapper(httpServletRequest, "x-source-ip", request.getRemoteAddr());

        chain.doFilter(wrappedRequest, response);
    }

    /**
     * Called by the web container to indicate to a filter that it is being taken out of service.
     *
     * <p>This method is only called once all threads within the filter's
     * doFilter method have exited or after a timeout period has passed. After the web container calls this method, it
     * will not call the doFilter method again on this instance of the filter.
     *
     * <p>This method gives the filter an opportunity to clean up any
     * resources that are being held (for example, memory, file handles, threads) and make sure that any persistent
     * state is synchronized with the filter's current state in memory.
     */
    @Override
    public void destroy() {
        // ignore
    }

    public class OverrideHeaderWrapper extends HttpServletRequestWrapper {

        private final String headerValue;
        private final String headerName;

        OverrideHeaderWrapper(HttpServletRequest request, String headerName, String headerValue) {
            super(request);
            this.headerName = headerName;
            this.headerValue = headerValue;
        }

        public String getHeader(String name) {
            if (name.equals(headerName)) {
                return headerValue;
            } else {
                return super.getHeader(name);
            }
        }

        public Enumeration<String> getHeaderNames() {
            List<String> names = Collections.list(super.getHeaderNames());
            names.add(headerName);
            return Collections.enumeration(names);
        }

        @Override
        public Enumeration<String> getHeaders(String name) {
            if (name.equals(headerName)) {
                return Collections.enumeration(Collections.singletonList(headerValue));
            } else {
                return super.getHeaders(name);
            }
        }
    }
}
